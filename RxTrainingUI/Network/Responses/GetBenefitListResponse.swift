//
//  GetBenefitListResponse.swift
//  RxTrainingUI
//
//  Created by Jirson Alexander Tavera Varon on 20/11/19.
//  Copyright © 2019 Devecol. All rights reserved.
//

import Foundation


struct GetBenefitResultListResponse: Codable {
    let message: String?
    let result: String
    let dataCount: Int
    let status: Bool
    let code: Int
    let exception: String?
    let data: [Payload]
    
    struct Payload: Codable {
        let alliedId: Int
        let alliedName: String
        let benefitId: Int
        let title: String
        let lead: String
        let discount: String
        let imageBenefit: ImageData
        let imageLogo: ImageData
        let imageHome: ImageData
        let categoryId: Int
        let categoryName: String
        let segmentId: Int
    }
    
    struct ImageData: Codable {
        let small: String
        let medium: String
        let original: String
    }
}
