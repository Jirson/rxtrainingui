//
//  API.swift
//  RxTrainingUI
//
//  Created by Jirson Alexander Tavera Varon on 20/11/19.
//  Copyright © 2019 Devecol. All rights reserved.
//

import Foundation
import Alamofire

enum API {
    case getBenefitResultList(parameters: [String: Any])
    
    var path: String {
        switch self {
            case .getBenefitResultList: return "benefits/list"
        }
    }
    
    /// The headers to be used in the request.
    var headers: HTTPHeaders? {
        switch self {
        case .getBenefitResultList:
            return ["Content-Type": "application/json"]
        }
    }
    
    /// The HTTP method used in the request.
    var method: HTTPMethod {
        switch self {
        case .getBenefitResultList:
            return .get
        }
    }
    var parameters: [String: Any] {
        var params: [String: Any] = [:]
        switch self {
        case .getBenefitResultList(let parameters):
            params = parameters
        }
        return params
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .getBenefitResultList:
            return URLEncoding.default
        }
    }
}
