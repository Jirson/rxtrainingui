//
//  Network.swift
//  RxTrainingUI
//
//  Created by Jirson Alexander Tavera Varon on 20/11/19.
//  Copyright © 2019 Devecol. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire


struct ErrorResponse: Codable {
    let code: Int?
    let data: String?
    let message: String?
    let result: String
}

struct CVError: Error {
    public let code: Int
    public let message: String
    public let name: String
}

struct ErrorHelper {
    
    static func handleError(responseData: Data?, responseError: Error?) -> CVError {
        
        var apiError = CVError(code: 401, message: "Unknown Error", name: "Error")
        
        // Try to decode the response data from server
        do {
            let responseDecode = try JSONDecoder().decode(ErrorResponse.self, from: responseData ?? Data())
            apiError = CVError(code: responseDecode.code ?? 500, message: responseDecode.message ?? "Try again in a time", name: responseDecode.result)
        } catch let errorDecode {
            print(errorDecode.localizedDescription)
            // If there's not a response codable from server
            apiError = getErrorFromNetwork(apiError: apiError, responseError: responseError)
        }
        
        return apiError
    }
    
    static func getErrorFromNetwork(apiError: CVError, responseError: Error?) -> CVError {
        var apiError = apiError
        if let err = responseError as? AFError {
            apiError = CVError(code: err.responseCode ?? 0, message: err.localizedDescription, name: "Error")
        } else if let err = responseError as? URLError { //TimedOut
            apiError = CVError(code: err.errorCode, message: err.localizedDescription, name: "Error")
        } else {
            apiError = CVError(code: 401, message: "Unknown Error", name: "Error")
        }
        return apiError
    }
}


class Network {
    
    //MARK: - Properties
    static let sharedInstance = Network()
    let baseUrl: String = "https://clubvivamos.ceet.co/club-suscriptores-api/v1/"
    private lazy var alamoFireManager: SessionManager? = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 20
        configuration.timeoutIntervalForResource = 20
        let alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        return alamoFireManager
    }()
    
    //MARK: - Initializer
    private init() {}
    
    func request<T: Codable>(_ endPoint: API) -> Observable<T> {
        let absolutePath = "\(self.baseUrl)\(endPoint.path)"
        return Observable.create { [endPoint] (observer) -> Disposable in
            self.alamoFireManager?.request(absolutePath, method: endPoint.method, parameters: endPoint.parameters, encoding: endPoint.encoding, headers: endPoint.headers).validate().responseJSON { [observer] (response) in
                print(response)
                switch response.result {
                case .success:
                    do {
                        guard let data = response.data else {
                            observer.onError(response.error ?? CVError(code: 404, message: "no data", name: ""))
                            return
                        }
                        let responseDecode = try JSONDecoder().decode(T.self, from: data)
                        observer.onNext(responseDecode)
                    } catch let error {
                        let apiError = ErrorHelper.handleError(responseData: response.data, responseError: response.result.error)
                        observer.onError(apiError)
                    }
                case .failure(let error):
                    let apiError = ErrorHelper.handleError(responseData: response.data, responseError: response.result.error)
                    observer.onError(apiError)
                }
            }
            return Disposables.create()
        }
    }
    
}
