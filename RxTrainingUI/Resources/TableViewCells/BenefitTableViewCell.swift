//
//  BenefitTableViewCell.swift
//  RxTrainingUI
//
//  Created by Jirson Alexander Tavera Varon on 20/11/19.
//  Copyright © 2019 Devecol. All rights reserved.
//

import UIKit

class BenefitTableViewCell: UITableViewCell {

    @IBOutlet weak var alliedNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
