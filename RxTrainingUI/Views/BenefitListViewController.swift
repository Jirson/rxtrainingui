//
//  BenefitListViewController.swift
//  RxTrainingUI
//
//  Created by Jirson Alexander Tavera Varon on 20/11/19.
//  Copyright © 2019 Devecol. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BenefitListViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var benefitListTableView: UITableView!
    @IBOutlet weak var tursimoButton: UIButton!
    
    //MARK: - Properties
    private let viewModel: BenefitListViewModel = BenefitListViewModel(service: BenefitService())
    private let disposeBag = DisposeBag()
    private var counter: Int = 0
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    private func setupUI() {
        self.benefitListTableView.register(UINib(nibName: "BenefitTableViewCell", bundle: nil), forCellReuseIdentifier: "Benefit_TableView_Cell")
        self.viewModel.benefitsLoaded.bind(to: self.benefitListTableView.rx.items(cellIdentifier: "Benefit_TableView_Cell", cellType: BenefitTableViewCell.self)) { row, model, cell in
            print("cell \(row) with alliedName \(model.alliedName)")
            cell.alliedNameLabel.text = "\(model.id) from \(model.alliedName)"
        }.disposed(by: self.disposeBag)
        
        self.benefitListTableView.rx.modelSelected(Benefit.self).subscribe(onNext: { benefit in
            print("Has seleccionado \(benefit.alliedName) con el id \(benefit.id)")
        }).disposed(by: self.disposeBag)
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (currentTimer) in
            self.counter += 1
            print(self.counter)
            if self.counter == 5 {
                self.viewModel.loadBenefits(categoryId: 5)
            }
        }
        
        self.tursimoButton.rx.tap.bind {
            print("button tapped")
            self.viewModel.loadBenefits(categoryId: 4)
        }.disposed(by: self.disposeBag)
    }

}
