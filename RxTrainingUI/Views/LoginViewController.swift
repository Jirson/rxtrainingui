//
//  ViewController.swift
//  RxTrainingUI
//
//  Created by Jirson Alexander Tavera Varon on 19/11/19.
//  Copyright © 2019 Devecol. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var warningLabel: UILabel!
    
    private let viewModel = LoginViewModel()
    fileprivate let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }

    private func setupUI() {
        print("LoginViewController - setupUI")
        self.bindViewModel()
    }
    
    private func bindViewModel() {
        self.emailTextField.rx.text.orEmpty.map { $0.isValidEmail }.subscribe(onNext: { isValid in
            print("email is \(isValid ? "" : "Not") Valid")
            self.warningLabel.text = "email is \(isValid ? "" : "Not") Valid"
        }).disposed(by: self.disposeBag)
    }
}
