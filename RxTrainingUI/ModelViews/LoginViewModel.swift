//
//  LoginViewModel.swift
//  RxTrainingUI
//
//  Created by Jirson Alexander Tavera Varon on 20/11/19.
//  Copyright © 2019 Devecol. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class LoginViewModel {
    
    var emailData: BehaviorRelay<String> = BehaviorRelay(value: "")
    fileprivate let disposeBag = DisposeBag()
    
    init() {
        print("LoginViewMOdel intializar")
        emailData.subscribe(onNext: {value in
            print("LoginViewModel \(value)")
        }).disposed(by: self.disposeBag)
    }
}
