//
//  BenefitListViewModel.swift
//  RxTrainingUI
//
//  Created by Jirson Alexander Tavera Varon on 20/11/19.
//  Copyright © 2019 Devecol. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class BenefitListViewModel {
    
    //MARK: - Properties
    //Variable de tipo BehaviourRelay que emite datos incluso el elemento antes de la suscripción cuando hay una primera suscripción
    /*var benefits: BehaviorRelay<[Benefit]> = BehaviorRelay(value: [
        Benefit(id: 1, alliedName: "Marvel"),
        Benefit(id: 2, alliedName: "DC"),
        Benefit(id: 3, alliedName: "Disnet")
    ])*/
    
    var service: BenefitService
    let disposeBag = DisposeBag()
    let benefitsLoadedWithError: PublishSubject<Error> = PublishSubject<Error>()
    var benefitsLoaded: BehaviorSubject = BehaviorSubject<[Benefit]>(value: [Benefit(id: 0, alliedName: "Initital allied")])
    
    init(service: BenefitService) {
        self.service = service
    }
    
    func loadBenefits(categoryId: Int) {
        self.useCase(params: ["from": 1, "size": 10, "categoryId": categoryId]).subscribe(onNext: { benefitList in
            self.benefitsLoaded.onNext(benefitList)
        }, onError: { error in
            self.benefitsLoadedWithError.onNext(error)
        }).disposed(by: self.disposeBag)
    }
    
    func useCase(params: [String: Any]) -> Observable<[Benefit]> {
        return BenefitService.getBenefits(params: params).map { (response) -> [Benefit] in
            var benefitListFromAPI = [Benefit]()
            for data in response.data {
                benefitListFromAPI.append(Benefit(id: data.alliedId, alliedName: data.alliedName))
            }
            return benefitListFromAPI
        }
    }
}

class BenefitService {
    init() {}
    
    static func getBenefits(params: [String: Any]) -> Observable<GetBenefitResultListResponse> {
        return Network.sharedInstance.request(.getBenefitResultList(parameters: params))
    }
}
