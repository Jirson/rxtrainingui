//
//  Benefit.swift
//  RxTrainingUI
//
//  Created by Jirson Alexander Tavera Varon on 20/11/19.
//  Copyright © 2019 Devecol. All rights reserved.
//

import Foundation

struct Benefit {
    let id: Int
    let alliedName: String
}
